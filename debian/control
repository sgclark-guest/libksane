Source: libkf5sane
Section: libs
Priority: optional
Maintainer: Debian/Kubuntu Qt/KDE Maintainers <debian-qt-kde@lists.debian.org>
Uploaders: Sune Vuorela <sune@debian.org>,
           Maximiliano Curia <maxy@debian.org>,
           Scarlett Moore <sgmoore@kde.org>,
Build-Depends: cmake (>= 3.0.0~),
               debhelper (>= 12~),
               extra-cmake-modules (>= 5.30.0~),
               libkf5i18n-dev (>= 5.37.0~),
               libkf5textwidgets-dev (>= 5.37.0~),
               libkf5wallet-dev (>= 5.37.0~),
               libkf5widgetsaddons-dev (>= 5.37.0~),
               libsane-dev,
               libx11-dev,
               pkg-kde-tools (>= 0.12),
               qtbase5-dev (>= 5.7.0~),
Standards-Version: 4.3.0
Homepage: https://phabricator.kde.org/source/libksane/
Vcs-Browser: https://salsa.debian.org/qt-kde-team/kde/libksane
Vcs-Git: https://salsa.debian.org/qt-kde-team/kde/libksane.git

Package: libkf5sane-data
Architecture: all
Depends: ${misc:Depends}
Breaks: libksane-data, ${kde-l10n:all}
Replaces: libksane-data, ${kde-l10n:all}
Description: scanner library (data files)
 The KDE scanner library provides an API and widgets for using scanners and
 other imaging devices supported by SANE.
 .
 This package contains data files used by the library.

Package: libkf5sane-dev
Section: libdevel
Architecture: any
Depends: libkf5sane5 (= ${binary:Version}),
         qtbase5-dev (>= 5.7.0~),
         ${misc:Depends},
Description: scanner library development headers
 The KDE scanner library provides an API and widgets for using scanners and
 other imaging devices supported by SANE.
 .
 This package contains the scanner library development files.

Package: libkf5sane5
Architecture: any
Depends: libkf5sane-data (= ${source:Version}),
         ${misc:Depends},
         ${shlibs:Depends},
Description: scanner library (runtime)
 The KDE scanner library provides an API and widgets for using scanners and
 other imaging devices supported by SANE.
 .
 This package contains the shared library.
